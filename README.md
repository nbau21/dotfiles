## Workflow

## Packages

### Backup list of packages

Run the following to generate list Arch packages currently installed:

```
make archlinuxpkglist
```

### Install packages

To install packages, run:

```
make archrestore
```

### Additional

To install completions for zsh, run:

```
sudo cp --remove-destination ./.config/.zsh/functions/alacritty-completions.zsh /usr/share/zsh/functions/Completion/X/_alacritty
```

