UNAME := $(shell uname)

config_dir = ${HOME}/.config/
ssh_dir = ${HOME}/.ssh/
ssh_id_rsa = ${ssh_dir}id_rsa

restore:
ifeq ($(UNAME), Linux)
restore: linuxdotfiles generatenewsshkey messages
else ifeq ($(UNAME), Darwin)
	$(error Restore on macs not supported..)
endif

archrestore: archinstall restore

archbackup: archpkgs

## Copy over .dotfiles for linux machines
linuxdotfiles: | cleandotfiles $(config_dir)
	ln -vsf ${PWD}/.gitconfig   ${HOME}/.gitconfig
	ln -vsf ${PWD}/.zshrc   ${HOME}/.zshrc
	ln -vsf ${PWD}/.xinitrc   ${HOME}/.xinitrc
	ln -vsf ${PWD}/.newsboat/   ${HOME}/
	ln -vsf ${PWD}/.elinks/   ${HOME}/
	ln -vsf ${PWD}/.emacs.d/   ${HOME}/
	ln -vsf ${PWD}/.config/kak/   ${HOME}/.config/
	ln -vsf ${PWD}/.config/qutebrowser/   ${HOME}/.config/
	ln -vsf ${PWD}/.config/awesome/   ${HOME}/.config/
	ln -vsf ${PWD}/.config/.zsh/   ${HOME}/.config/
	ln -vsf ${PWD}/.config/alacritty/   ${HOME}/.config/
	sudo ln -vsf ${PWD}/.config/.zsh/functions/alacritty_completion.zsh   /usr/share/zsh/functions/Completion/X/_alacritty

cleandotfiles:
	rm -rf ${HOME}/.config/
	rm -rf ${HOME}/.zshrc
	rm -rf ${HOME}/.xinitrc
	rm -rf ${HOME}/.newsboat/
	rm -rf ${HOME}/.elinks/
	rm -rf ${HOME}/.emacs.d/

generatenewsshkey: $(ssh_dir)
ifeq (,$(wildcard ${ssh_dir}))
	ssh-keygen -f ${ssh_id_rsa} -N '' -t rsa -b 4096 -C 'noel@noynoy.org'
	eval `ssh-agent -s` && ssh-add ${ssh_id_rsa}
else
	@echo "{HOME}/.ssh/ exists already. Will not overwrite. Skipping.."
endif

installyay:
ifeq (,$(wildcard /usr/bin/yay))
	sudo pacman -S --noconfirm --needed git
	git clone https://aur.archlinux.org/yay.git
	cd ${PWD}/yay/;	makepkg -si
	rm -rf ${PWD}/yay/
endif

archinstall: installyay ## Install official packages in Arch Linux
	yay -Syu
	yay -S --force --noconfirm --needed `cat ${PWD}/Packages`

archpkgs: installyay ## List explicitly installed packages for Arch Linux
	yay -Qet > Packages
	systemctl enable acpid ## Need to enable acpid for battery and volume widgets

messages: ## Logs
	@echo "==="
	@echo "==="
	@echo "==="
	@echo "Don't forget to copy your id_rsa.pub to your vcs of choice (github, gitea, gitlab, etc)."
	@echo "==="
	@echo "==="
	@echo "==="

$(ssh_dir):
	$(call create_folder, $@)

$(config_dir):
	$(call create_folder, $@)

define create_folder
	@echo "${1} does not exist. Creating.."
	mkdir ${1}
endef

